﻿using System;
using System.Drawing;
using SdlDotNet.Graphics;
using SdlDotNet.Core;
using SdlDotNet.Input;

namespace FlapiBird
{
    class Game
    {
        private const int DIMX = 800;
        private const int DIMY = 600;
        private const int INI = 0;
        private const int MAXJUMP = 10;
        private const double SECONDS = 0.01;
        private const int FPS = 100;
        private const int ANIMATIONSPEED = 15;
        private const double GRAVITY = 9.8;
        private Surface f_videoSurface;
        private Surface f_rightBack;
        private Surface f_leftBack;
        private Object f_flapi;
        private Object f_tubeUp;
        private Object f_tubeDown;
        private bool f_crash = false;
        private bool f_pause = true;
        private bool f_arrowUp = false;
        private int f_frames = 0;
        private int f_backPosRight = 800;
        private int f_backPosLeft = 0;
        private Random f_random = new Random();
        private SdlDotNet.Graphics.Font f_font = new SdlDotNet.Graphics.Font("candy shop.ttf", 12);

        public Game()
        {
            // Video surface declaration
            f_videoSurface = Video.SetVideoMode(DIMX, DIMY, false, false, false, true);

            // Background images declaration
            f_leftBack = (new Surface(@"backgroundE.png")).Convert(f_videoSurface, true, false);
            f_rightBack = (new Surface(@"backgroundD.png")).Convert(f_videoSurface, true, false);

            // Main object(flapi) declaration
            #region Flapi declaration
            f_flapi = new Object(new SpriteSheet(new Surface(@"FlyGuy.png").Convert(f_videoSurface, true, true), 97, 104, 388, 104), 100, 250);
            f_flapi.SS.Img.Transparent = true;
            f_flapi.SS.Img.TransparentColor = Color.FromArgb(180, 255, 213);
            #endregion

            // Tube object declaration
            // Up tube
            f_tubeUp = new Object(new SpriteSheet(new Surface(@"tubeUp.png").Convert(f_videoSurface, true, true), 200, 300, 200, 300), 500, -150);
            f_tubeUp.SS.Img.Transparent = true;
            f_tubeUp.SS.Img.TransparentColor = Color.FromArgb(255, 0, 255);
            // Down tube
            f_tubeDown = new Object(new SpriteSheet(new Surface(@"TubeDown.png").Convert(f_videoSurface, true, true), 200, 300, 200, 300), 500, 450);
            f_tubeDown.SS.Img.Transparent = true;
            f_tubeDown.SS.Img.TransparentColor = Color.FromArgb(255, 0, 255);

            // Choice of FPS
            Events.Fps = FPS;

            // Events calling
            Events.Tick += new EventHandler<TickEventArgs>(ApplicationTickEventHandler);
            Events.KeyboardDown += new EventHandler<KeyboardEventArgs>(KeyboardDownEventHandler);
            Events.KeyboardUp += new EventHandler<KeyboardEventArgs>(KeyboardUpEventHandler);

            // Game start
            Events.Run();
        }
        private void ApplicationTickEventHandler(object sender, TickEventArgs args)
        {
            // If game isn't in pause keeps going.
            if (!f_pause)
            {
                // If player didn't crashed keeps going.
                if (!f_crash)
                {
                    // Call animation movement functions
                    f_frames++;
                    AnimationMovement(f_flapi);
                    if (f_frames == ANIMATIONSPEED) f_frames = 0;

                    // Call object movement functions
                    MainObjMovement();
                    TubeObjMovement();
                    Gravity();

                    // Call collision function
                    Collision();

                    // Blit the spritesheets to video surface.
                    // Background
                    f_videoSurface.Blit(f_leftBack, BackPostion(ref f_backPosLeft));
                    f_videoSurface.Blit(f_rightBack, BackPostion(ref f_backPosRight));
                    // Main object(flapi)
                    f_videoSurface.Blit(f_flapi.SS.Img, ObjectPosition(f_flapi), Animation(f_flapi));
                    // Tube object
                    // Up tube
                    f_videoSurface.Blit(f_tubeUp.SS.Img, ObjectPosition(f_tubeUp), Animation(f_tubeUp));
                    // Down tube
                    f_videoSurface.Blit(f_tubeDown.SS.Img, ObjectPosition(f_tubeDown), Animation(f_tubeDown));
                    // Text
                    f_videoSurface.Blit(f_font.Render(f_flapi.PosX.ToString(new string('0', 3)) + ", " + f_flapi.PosY.ToString(new string('0', 3)), Color.Black), new Point(400, 10));

                    // Video surface update.
                    f_videoSurface.Update();
                }
                // If player crashed.
                else
                {
                    // Crash code
                }
            }
        }
        private void KeyboardDownEventHandler(object sender, KeyboardEventArgs args)
        {
            // When key is pressed down.
            switch (args.Key)
            {
                case Key.UpArrow:
                    f_arrowUp = true;
                    f_flapi.Jump = 0;
                    f_flapi.FallSpeed = 0;
                    if(f_pause) f_pause = false;
                    break;
                case Key.Space:
                    f_pause = !f_pause;
                    break;
                case Key.Escape:
                    Events.QuitApplication();
                    break;
            }
        }
        private void KeyboardUpEventHandler(object sender, KeyboardEventArgs args)
        {
            // When key is pressed up.
            switch (args.Key)
            {
                case Key.Return:
                    Reset();
                    break;
                case Key.Escape:
                    Events.QuitApplication();
                    break;
            }
        }
        private void MainObjMovement()
        {
            //f_flapi.Mov = false;
            if (f_arrowUp)
            {
                if (f_flapi.Jump != MAXJUMP)
                {
                    f_flapi.PosY -= 10;
                    f_flapi.Jump += 1;
                }
            }
            if (f_flapi.PosY + f_flapi.SS.ObjDimY >= DIMY || f_flapi.PosY <= INI) f_crash = true;
        }
        private void TubeObjMovement()
        {
            if (f_tubeDown.PosX == -200)
            {
                int v_tubePos = f_random.Next(20, 300);
                f_tubeDown.PosX = 800;
                f_tubeDown.PosY = 300 + v_tubePos;
                f_tubeUp.PosX = 800;
                f_tubeUp.PosY = -300 + v_tubePos;
            }
            else
            {
                f_tubeDown.PosX -= 10;
                f_tubeUp.PosX -= 10;
            }
        }
        private void Gravity()
        {
            if (f_flapi.Jump == MAXJUMP)
            {
                f_flapi.FallSpeed += GRAVITY * SECONDS;
                f_flapi.PosY = f_flapi.PosY + Convert.ToInt32(Math.Round(f_flapi.FallSpeed));
            }
        }
        private void Collision()
        {
            if ((f_flapi.PosX + f_flapi.SS.ObjDimX) > f_tubeUp.PosX
                && (f_flapi.PosX + f_flapi.SS.ObjDimX) < (f_tubeUp.PosX + f_tubeUp.SS.ObjDimX)
                && f_flapi.PosY < (f_tubeUp.PosY + f_tubeUp.SS.ObjDimY)
                && f_flapi.PosY > f_tubeUp.PosY)
                f_crash = true;
            else if((f_flapi.PosX + f_flapi.SS.ObjDimX) > f_tubeDown.PosX
                && (f_flapi.PosX + f_flapi.SS.ObjDimX) < (f_tubeDown.PosX + f_tubeDown.SS.ObjDimX) 
                && f_flapi.PosY + f_flapi.SS.ObjDimY < (f_tubeDown.PosY + f_tubeDown.SS.ObjDimY)
                && f_flapi.PosY + f_flapi.SS.ObjDimY > f_tubeDown.PosY)
                f_crash = true;
        }
        private void Reset()
        {
            f_flapi.PosX = 100;
            f_flapi.PosY = 250;
            f_tubeUp.PosX = 500;
            f_tubeUp.PosY = -150;
            f_tubeDown.PosX = 500;
            f_tubeDown.PosY = 450;
            f_pause = true;
            f_crash = false;

            // Blit the spritesheets to video surface.
            // Background
            f_videoSurface.Blit(f_leftBack, BackPostion(ref f_backPosLeft));
            f_videoSurface.Blit(f_rightBack, BackPostion(ref f_backPosRight));
            // Main object(flapi)
            f_videoSurface.Blit(f_flapi.SS.Img, ObjectPosition(f_flapi), Animation(f_flapi));
            // Tube object
            // Up tube
            f_videoSurface.Blit(f_tubeUp.SS.Img, ObjectPosition(f_tubeUp), Animation(f_tubeUp));
            // Down tube
            f_videoSurface.Blit(f_tubeDown.SS.Img, ObjectPosition(f_tubeDown), Animation(f_tubeDown));
            // Text
            f_videoSurface.Blit(f_font.Render(f_flapi.PosX.ToString(new string('0', 3)) + ", " + f_flapi.PosY.ToString(new string('0', 3)), Color.Black), new Point(400, 10));

            // Video surface update.
            f_videoSurface.Update();
        }
        private void AnimationMovement(Object p_obj)
        {
            if (f_frames == ANIMATIONSPEED)
            {
                if (p_obj.SS.ImgPosX != p_obj.SS.ImgDimX - p_obj.SS.ObjDimX
                    && p_obj.SS.ObjDimX != p_obj.SS.ImgDimX)
                {
                    p_obj.SS.ImgPosX += p_obj.SS.ObjDimX;
                }
                else
                {
                    p_obj.SS.ImgPosX = 0;
                    if (p_obj.SS.ImgPosY != p_obj.SS.ImgDimY - p_obj.SS.ObjDimY
                        && p_obj.SS.ObjDimY != p_obj.SS.ImgDimY)
                    {
                        p_obj.SS.ImgPosY += p_obj.SS.ObjDimY;
                    }
                    else p_obj.SS.ImgPosY = 0;
                }
            }
        }
        private Rectangle Animation(Object p_obj)
        {
            return new Rectangle(new Point(p_obj.SS.ImgPosX, p_obj.SS.ImgPosY), new Size(p_obj.SS.ObjDimX, p_obj.SS.ObjDimY));
        }
        private Point BackPostion(ref int p_backPos)
        {
            if (p_backPos == -800) p_backPos = 800;
            else p_backPos -= 1;
            return new Point(p_backPos, INI);
        }
        private Point ObjectPosition(Object p_obj)
        {
            return new Point(p_obj.PosX, p_obj.PosY);
        }
    }
}
