﻿using SdlDotNet.Graphics;

namespace FlapiBird
{
    class SpriteSheet
    {
        private Surface f_img;
        private int f_imgPosX;
        private int f_imgPosY;
        private int f_objDimX;
        private int f_objDimY;
        private int f_imgDimX;
        private int f_imgDimY;

        /// <summary>
        /// SpriteSheet declaration with default initial position(0,0).
        /// </summary>
        /// <param name="p_img">Image</param>
        /// <param name="p_objDimX">Object X dimension</param>
        /// <param name="p_objDimY">Object Y dimension</param>
        /// <param name="p_imgDimX">Image X dimension</param>
        /// <param name="p_imgDimY">Image Y dimension</param>
        public SpriteSheet(Surface p_img, int p_objDimX, int p_objDimY, int p_imgDimX, int p_imgDimY)
        {
            f_img = p_img;
            f_objDimX = p_objDimX;
            f_objDimY = p_objDimY;
            f_imgDimX = p_imgDimX;
            f_imgDimY = p_imgDimY;
            f_imgPosX = 0;
            f_imgPosY = 0;
        }
        /// <summary>
        /// SpriteSheet declaration assigning initial position(0,0).
        /// </summary>
        /// <param name="p_img">Image</param>
        /// <param name="p_objDimX">Object X dimension</param>
        /// <param name="p_objDimY">Object Y dimension</param>
        /// <param name="p_imgDimX">Image X dimension</param>
        /// <param name="p_imgDimY">Image Y dimension</param>
        /// <param name="p_imgPosX">Image X position</param>
        /// <param name="p_imgPosY">Image Y position</param>
        public SpriteSheet(Surface p_img, int p_objDimX, int p_objDimY, int p_imgDimX, int p_imgDimY, int p_imgPosX, int p_imgPosY)
        {
            f_img = p_img;
            f_objDimX = p_objDimX;
            f_objDimY = p_objDimY;
            f_imgDimX = p_imgDimX;
            f_imgDimY = p_imgDimY;
            f_imgPosX = p_imgPosX;
            f_imgPosY = p_imgPosY;
        }
        #region Propietats
        /// <summary>
        /// Image url.
        /// </summary>
        public Surface Img
        {
            get { return f_img; }
            set { f_img = value; }
        }
        /// <summary>
        /// Object X dimension
        /// </summary>
        public int ObjDimX
        {
            get { return f_objDimX; }
            set { f_objDimX = value; }
        }
        /// <summary>
        /// Object Y dimension
        /// </summary>
        public int ObjDimY
        {
            get { return f_objDimY; }
            set { f_objDimY = value; }
        }
        /// <summary>
        /// Image X dimension
        /// </summary>
        public int ImgDimX
        {
            get { return f_imgDimX; }
            set { f_imgDimX = value; }
        }
        /// <summary>
        /// Image Y dimension
        /// </summary>
        public int ImgDimY
        {
            get { return f_imgDimY; }
            set { f_imgDimY = value; }
        }
        /// <summary>
        /// Image X position
        /// </summary>
        public int ImgPosX
        {
            get { return f_imgPosX; }
            set { f_imgPosX = value; }
        }
        /// <summary>
        /// Image Y position
        /// </summary>
        public int ImgPosY
        {
            get { return f_imgPosY; }
            set { f_imgPosY = value; }
        }
        #endregion
    }
}