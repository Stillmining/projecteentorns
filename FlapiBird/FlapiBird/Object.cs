﻿namespace FlapiBird
{
    class Object
    {
        private SpriteSheet f_ss;
        private int f_posX;
        private int f_posY;
        private int f_jump;
        private double f_fallSpeed;
        private bool f_down;
        private bool f_up;

        /// <summary>
        /// Object declaration with default XY position.
        /// </summary>
        /// <param name="ss">Sprite sheet</param>
        public Object(SpriteSheet p_ss)
        {
            f_ss = p_ss;
            f_down = false;
            f_up = false;
            f_jump = 10;
            f_fallSpeed = 0;
            f_posX = 0;
            f_posY = 0;
        }

        /// <summary>
        /// Object declaration choosing initial XY position of object.
        /// </summary>
        /// <param name="ss">Sprite sheet</param>
        /// <param name="posX">Initial X position</param>
        /// <param name="posY">Initial Y position</param>
        public Object(SpriteSheet p_ss, int p_posX, int p_posY)
        {
            f_ss = p_ss;
            f_posX = p_posX;
            f_posY = p_posY;
            f_down = false;
            f_up = false;
            f_jump = 10;
            f_fallSpeed = 0;
        }

        #region Propietats
        public SpriteSheet SS
        {
            get { return f_ss; }
            set { f_ss = value; }
        }
        public bool Down
        {
            get { return f_down; }
            set { f_down = value; }
        }
        public bool Up
        {
            get { return f_up; }
            set { f_up = value; }
        }
        public int PosX
        {
            get { return f_posX; }
            set { f_posX = value; }
        }
        public int PosY
        {
            get { return f_posY; }
            set { f_posY = value; }
        }
        public double FallSpeed
        {
            get { return f_fallSpeed; }
            set { f_fallSpeed = value; }
        }
        public int Jump
        {
            get { return f_jump; }
            set { f_jump = value; }
        }
        #endregion
    }
}