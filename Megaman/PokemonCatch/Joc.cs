﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SdlDotNet.Graphics;
using SdlDotNet.Graphics.Sprites;
using SdlDotNet.Core;
using SdlDotNet;
using SdlDotNet.Input;

namespace PokemonCatch
{
    
    class Joc
    {
        public const string POKEBALL_ = "pokeball.PNG";
        public const string SUPERBALL_ = "superball.PNG";
        public const string ULTRABALL_ = "ultraball.PNG";
        public const string MASTERBALL_ = "masterball.PNG";
        Jugador ash;
        Jugador pika;
        Jugador pokeball;
        Sprite sFons;
        Sprite sJugador;
        Sprite sPokemon;
        Sprite sBall;
        Sprite [] sFinal;
        TextSprite pos;
        Random rand;
        Size mon;
        bool bola;
        bool final;
        int nFrame;
        public Joc()
        {
            nFrame = 0;
            Sprite pika1, pika2, pika3, pika4;
            rand = new Random();
            final = false;
            bola = false;
            mon = new Size(450, 450);
            ash = new Jugador("ash", 0, 0, 2, mon);
            pika = new Jugador("pika", 0, 0, 2, mon);
            pokeball = new Jugador("pokeball", 0, 0, 2, mon);
            Video.SetVideoMode(445, 447); //inicialitza el joc.
            sFons = new Sprite(new Surface("map.PNG"));
            sJugador = new Sprite(new Surface("ash.PNG"));//amb aixo aconseguirem que trobi la imatge a dins la carpeta del programa 
            sPokemon = new Sprite(new Surface("pika.PNG"));
            pika1 = new Sprite(new Surface("pika1.PNG"));
            pika2 = new Sprite(new Surface("pika2.PNG"));
            pika3 = new Sprite(new Surface("pika3.PNG"));
            pika4 = new Sprite(new Surface("pika4.PNG"));
            sFinal = new Sprite[] { pika1, pika2, pika3, pika4 };
            sJugador.Position = new Point(0, 0);
            sPokemon.Center = new Point(250, 250);

            //string posicio = ash.ToString();
            //SdlDotNet.Graphics.Font myfont = new SdlDotNet.Graphics.Font("ARCADE_N.ttf", 8);
            //pos = new TextSprite(posicio, myfont, Color.White);
            //pos.Center = new Point(100, 490);

            Video.Screen.Blit(sFons);
            Video.Screen.Blit(sJugador);
            Video.Screen.Blit(sPokemon);//estampa els sprites
            //Video.Screen.Blit(pos);

            Events.Quit += Events_Quit;
            Events.Tick += Events_Tick;  // quant sexecuti el tick va a dintre del events el tick crea un bucle ell mateix, cada lapse de milesimes de segon s'executa
            Events.Run();//fa que funcioni el events


        }

        void Events_Tick(object sender, TickEventArgs e)
        {
            if (!final)
            {
                // per controlar el robot manualment amb teclat
                if (Keyboard.IsKeyPressed(Key.UpArrow) || Keyboard.IsKeyPressed(Key.W)) //cada cop que es presiona la tecla andalt el robot es moura cap adalt                
                {
                    int novaY = sJugador.Y - 5;
                    if (novaY >= 0)
                    {
                        sJugador.Y = novaY;
                        ash.Y = novaY;
                        ash.Direccio = 0;
                    }
                }

                if (Keyboard.IsKeyPressed(Key.DownArrow) || Keyboard.IsKeyPressed(Key.S))
                {
                    int novaY = sJugador.Y + 5;
                    if (novaY <= (Video.Screen.Height - sJugador.Height))
                    {
                        sJugador.Y = novaY;
                        ash.Y = novaY;
                        ash.Direccio = 2;
                    }
                }

                if (Keyboard.IsKeyPressed(Key.LeftArrow) || Keyboard.IsKeyPressed(Key.A))
                {
                    int novaX = sJugador.X - 5;
                    if (novaX >= 0)
                    {
                        sJugador.X = novaX;
                        ash.X = novaX;
                        ash.Direccio = 3;
                    }

                    Point pos = sJugador.Position;
                    sJugador = new Sprite(new Surface("ash_inv.PNG"));
                    sJugador.Position = pos;
                }

                if (Keyboard.IsKeyPressed(Key.RightArrow) || Keyboard.IsKeyPressed(Key.D))
                {
                    int novaX = sJugador.X + 5;
                    if (novaX <= (Video.Screen.Width - sJugador.Width))
                    {
                        sJugador.X = novaX;
                        ash.X = novaX;
                        ash.Direccio = 1;
                    }
                    Point pos = sJugador.Position;
                    sJugador = new Sprite(new Surface("ash.PNG"));
                    sJugador.Position = pos;
                }
                if (Keyboard.IsKeyPressed(Key.Space))
                {
                    if (!bola)
                    {
                        int ball = rand.Next(0, 4);

                        Point offsetPos = new Point(10, 10);
                        Point pos = new Point(sJugador.Position.X + offsetPos.X, sJugador.Position.Y + offsetPos.Y);
                        switch (ball)
                        {
                            case 0:
                                sBall = new Sprite(new Surface(POKEBALL_));
                            break;
                            case 1:
                                sBall = new Sprite(new Surface(SUPERBALL_));
                                break;
                            case 2:
                                sBall = new Sprite(new Surface(ULTRABALL_));
                                break;
                            case 3:
                                sBall = new Sprite(new Surface(MASTERBALL_));
                                break;
                        }
                        sBall.Position = pos;
                        bola = true;
                        pokeball.X = sJugador.Position.X + offsetPos.X;
                        pokeball.Y = sJugador.Position.Y + offsetPos.Y;
                        pokeball.Direccio = ash.Direccio;
                    }
                }
                if (bola)
                {

                    pokeball.step(1);
                    sBall.Y = pokeball.Y;
                    sBall.X = pokeball.X;
                    if (pokeball.Moviments >= 100)
                    {
                        bola = false;
                        sBall = null;
                        pokeball = new Jugador("pokeball", 0, 0, 2, mon);
                    }
                }
                pika.step();
                if (pika.Direccio == 1)
                {
                    Point posPika = sPokemon.Position;
                    sPokemon = new Sprite(new Surface("pika.PNG"));
                    sPokemon.Position = posPika;
                }
                if (pika.Direccio == 3)
                {
                    Point posPika = sPokemon.Position;
                    sPokemon = new Sprite(new Surface("pika_inv.PNG"));
                    sPokemon.Position = posPika;
                }
                sPokemon.Y = pika.Y;
                sPokemon.X = pika.X;
                System.Threading.Thread.Sleep(20);

                Video.Screen.Blit(sFons);
                
                Video.Screen.Blit(sPokemon);//estampa els sprites
                Video.Screen.Blit(sJugador);
                if (bola)
                {
                    Video.Screen.Blit(sBall);
                }
                Video.Screen.Update();
                if (bola)
                {
                    if (!(((sBall.Center.X < sPokemon.Center.X - 10 || sBall.Center.X > sPokemon.Center.X + 10) || (sBall.Center.Y < sPokemon.Center.Y - 10 || sBall.Center.Y > sPokemon.Center.Y + 10))))
                    {
                        final = true;
                        System.Threading.Thread.Sleep(1500);
                    }
                }

            }
            else
            {
                Video.Screen.Fill(Color.Black);
                Video.Screen.Blit(sFinal[nFrame]);
                string fi = "MOLT BE!! HAS CAPTURAT EL POKEMON";
                SdlDotNet.Graphics.Font myfont2 = new SdlDotNet.Graphics.Font("ARCADE_N.ttf", 8);
                TextSprite missatge = new TextSprite(fi, myfont2, Color.Black);
                missatge.Center = new Point (220,100);
                Video.Screen.Blit(missatge);
                Video.Screen.Update();
                //aixo havia de servir per que la imatge del final fos un gif, però les imatges no tenen la mateixa mida i surt malament
                //nFrame++; 
                //if (nFrame == 4)
                //    nFrame = 0;
            }
        }
        private void Events_Quit(object sender, QuitEventArgs e)
        {
            Events.QuitApplication();
        }
    }
}
