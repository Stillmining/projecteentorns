﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuzzleDungeon
{
    class Jugador
    {
        public const int Nord = 0;
        public const int Est = 1;
        public const int Sud = 2;
        public const int Oest = 3;
        public Random r = new Random();

        private int direccio;
        private int x;
        private int y;
        private string nom;
        private Size mon;
        private int moviments;

        public Jugador()
        {
            nom = "R" + r.Next(0, 100);
            x = 0;
            y = 0;
            direccio = Sud;
            moviments = 0;
            mon = new Size(10, 10);
        }

        public Jugador(Size s)
        {
            nom = "R" + r.Next(0, 100);
            x = 0;
            y = 0;
            direccio = Sud;
            moviments = 0;
            mon = s;
        }


        public Jugador(string n, int posX, int posY, int d, Size s)
        {
            nom = n;
            direccio = d % 4;
            x = posX;
            y = posY;
            mon = s;
            moviments = 0;
        }
        public int X
        {
            get { return x; }
            set { x = value; }
        }

        public int Y
        {
            get { return y; }
            set { y = value; }
        }

        public int Moviments
        {
            get { return moviments; }

        }

        public override string ToString()
        {
            string text = "";
            text += "Soc " + nom + " estic (" + x + "," + y + ")";
            switch (direccio)
            {
                case Nord:
                    text += " N";
                    break;
                case Est:
                    text += " E";
                    break;
                case Sud:
                    text += " S";
                    break;
                case Oest:
                    text += " O";
                    break;
            }
            return text;
        }

        public void canviDireccio()
        {

            int novaDireccio = r.Next(0, 2);
            if (novaDireccio == 0) direccio--;
            else if (novaDireccio == 1) direccio++;
            direccio %= 4;
            if (direccio < 0) direccio = Oest;
        }

        public void step()
        {
            int accio = r.Next(0, 4);
            if (accio != 0)
            {
                switch (direccio)
                {
                    case Nord:
                        y = y - 5;
                        break;
                    case Est:
                        x = x + 5;
                        break;
                    case Sud:
                        Y = y + 5;
                        break;
                    case Oest:
                        x = x - 5;
                        break;
                }

                if (x < 0) x = mon.Width - 1;
                x = x % mon.Width;
                if (y < 0) y = mon.Height - 1;
                y = y % mon.Height;
                move();
            }
            else canviDireccio();

        }

        public void move()
        {
            moviments++;
        }
    }
}
