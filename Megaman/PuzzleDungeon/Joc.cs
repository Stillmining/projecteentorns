﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SdlDotNet.Graphics;
using SdlDotNet.Graphics.Sprites;
using SdlDotNet.Core;
using SdlDotNet;
using SdlDotNet.Input;

namespace PuzzleDungeon
{
    class Joc
    {
        Jugador robot;
        Sprite sRobot;
        Sprite sTresor;
        Sprite sFinal;
        public Joc()
        {
            Size mon = new Size(400, 400);
            robot = new Jugador("charli",0,0,2, mon);
            Video.SetVideoMode(500, 500); //inicialitza el joc.
            sRobot = new Sprite(new Surface("robot.PNG"));//amb aixo aconseguirem que trobi la imatge a dins la carpeta del programa 
            sTresor = new Sprite(new Surface("treasure.PNG"));
            sFinal = new Sprite(new Surface("gameover.BMP"));
            sFinal.Center = new Point(250, 250);
            sRobot.Position = new Point(0, 0);
            sTresor.Center = new Point(250, 250);

            string posicio = robot.ToString();
            SdlDotNet.Graphics.Font myfont = new SdlDotNet.Graphics.Font("ARCADE_N.ttf", 8);
            TextSprite pos = new TextSprite(posicio, myfont, Color.White);
            pos.Center = new Point(100, 490);


            Video.Screen.Fill(Color.Black); //pinta de negre la pantalla
            Video.Screen.Blit(sRobot);
            Video.Screen.Blit(sTresor);//estampa els sprites
            Video.Screen.Blit(pos);
            while ((sRobot.Center.X<sTresor.Center.X - 30 || sRobot.Center.X> sTresor.Center.X + 30) || (sRobot.Center.Y<sTresor.Center.Y - 30 || sRobot.Center.Y> sTresor.Center.Y + 30))
            {
               
                robot.step();
                sRobot.Y = robot.Y;
                sRobot.X = robot.X;                
                pos.Text = robot.ToString();
                Video.Screen.Fill(Color.Black);
                Video.Screen.Blit(sTresor);//estampa els sprites
                Video.Screen.Blit(sRobot);
                Video.Screen.Blit(pos);
                Video.Screen.Update();
                System.Threading.Thread.Sleep(20);
                //refresca la pantalla
            }
            System.Threading.Thread.Sleep(1500);
            //MessageBox.Show("He trobat el tresor en {0} moviments.", robot.Moviments);
            Video.Screen.Fill(Color.Black);
            Video.Screen.Blit(sFinal);
            string fi = "He trobat el tresor en " + robot.Moviments + " moviments.";
            SdlDotNet.Graphics.Font myfont2 = new SdlDotNet.Graphics.Font("ARCADE_N.ttf", 12);
            TextSprite missatge = new TextSprite(fi, myfont2, Color.Black);
            missatge.Center = sFinal.Center;
            Video.Screen.Blit(missatge);
            Video.Screen.Update();


            Events.Tick += Events_Tick;  // quant sexecuti el tick va a dintre del events el tick crea un bucle ell mateix, cada lapse de milesimes de segon s'executa
            Events.Run();//fa que funcioni el events
        }
    
    void Events_Tick(object sender, TickEventArgs e)
    {

        // per controlar el robot manualment amb teclat
        if (Keyboard.IsKeyPressed(Key.UpArrow)) //cada cop que es presiona la tecla amunt el robot es moura cap adalt                
        {
            int novaY = sRobot.Y - 5;
            if (novaY >= 0) sRobot.Y = novaY;
        }

        if (Keyboard.IsKeyPressed(Key.DownArrow))
        {
            int novaY = sRobot.Y + 5;
            if (novaY <= (Video.Screen.Height - sRobot.Height)) sRobot.Y = novaY;
        }

        if (Keyboard.IsKeyPressed(Key.LeftArrow))
        {
            int novaX = sRobot.X - 5;
            if (novaX >= 0) sRobot.X = novaX;
        }

        if (Keyboard.IsKeyPressed(Key.RightArrow))
        {
            int novaX = sRobot.X + 5;
            if (novaX <= (Video.Screen.Width - sRobot.Width)) sRobot.X = novaX;
        }
        Video.Screen.Update();

        }
    }
}


