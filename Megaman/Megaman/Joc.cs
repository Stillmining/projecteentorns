﻿using System;
using System.Drawing;
using System.Runtime.InteropServices;
using SdlDotNet.Graphics;
using SdlDotNet.Core;
using SdlDotNet.Input;

namespace Megaman
{
    class Joc
    {
        private Surface m_VideoSurface;
        private Surface m_BackgroundD;
        private Surface m_BackgroundE;
        private pj j1;
        private pj j2;
        private bool crash = false;
        private bool pause = false;
        private bool arrowRight = false;
        private bool arrowLeft = false;
        private bool arrowUp = false;
        private bool arrowDown = false;
        private bool arrowD = false;
        private bool arrowA = false;
        private bool arrowW = false;
        private bool arrowS = false;
        private double g = 9.8;
        private int posFonsD = 800;
        private int posFonsE = 0;
        private int frames = 0;
        private int velAnimacio = 10;
        private const int DIMX = 800;
        private const int DIMY = 600;
        private const int INI = 0;
        private const int SALTMAX = 10;
        private const double S = 0.01;
        private SdlDotNet.Graphics.Font myfont = new SdlDotNet.Graphics.Font("arial.ttf", 12);
        private enum Direccio
        {
            Left = 0,
            Up = 1,
            Right = 2,
        }

        public Joc()
        {
            
            
            m_VideoSurface = Video.SetVideoMode(DIMX, DIMY, false, false, false, true);
            m_BackgroundE = (new Surface(@"backgroundE.png")).Convert(m_VideoSurface, true, false);
            m_BackgroundD = (new Surface(@"backgroundD.png")).Convert(m_VideoSurface, true, false);

            #region Declaració jugadors
            j1 = new pj(new SpriteSheet(new Surface(@"FlyGuy.png").Convert(m_VideoSurface, true, true),
                                        388, 104, 97, 104),
                        new SpriteSheet(new Surface(@"FlyGuy.png").Convert(m_VideoSurface, true, true),
                                        388, 104, 97, 104),
                        new SpriteSheet(new Surface(@"FlyGuy.png").Convert(m_VideoSurface, true, true),
                                        388, 104, 97, 104), 2, 100, 250);

            j1.D.Img.Transparent = true;
            j1.D.Img.TransparentColor = Color.FromArgb(180, 255, 213);
            j1.E.Img.Transparent = true;
            j1.E.Img.TransparentColor = Color.FromArgb(180, 255, 213);
            j1.S.Img.Transparent = true;
            j1.S.Img.TransparentColor = Color.FromArgb(180, 255, 213);
            j1.Direccio = j1.PosDef;

            //j2 = new pj(new SpriteSheet(new Surface(@"moureMegamanDreta2.png").Convert(m_VideoSurface, true, true),
            //                            384, 48, 48, 48),
            //            new SpriteSheet(new Surface(@"moureMegamanEsquerre2.png").Convert(m_VideoSurface, true, true),
            //                            384, 48, 48, 48),
            //            new SpriteSheet(new Surface(@"saltaMegaman2.png").Convert(m_VideoSurface, true, true),
            //                            288, 96, 48, 48), 0, 464, 464);

            //j2.D.Img.Transparent = true;
            //j2.D.Img.TransparentColor = Color.FromArgb(255, 0, 255);
            //j2.E.Img.Transparent = true;
            //j2.E.Img.TransparentColor = Color.FromArgb(255, 0, 255);
            //j2.S.Img.Transparent = true;
            //j2.S.Img.TransparentColor = Color.FromArgb(255, 0, 255);
            //j2.Direccio = j2.PosDef;
            #endregion

            Events.Fps = 100;
            Events.Tick += new EventHandler<TickEventArgs>(ApplicationTickEventHandler);
            Events.KeyboardDown += new EventHandler<KeyboardEventArgs>(KeyboardDownEventHandler);
            Events.KeyboardUp += new EventHandler<KeyboardEventArgs>(KeyboardUpEventHandler);
            Events.Run();
        }

        private void ApplicationTickEventHandler(object sender, TickEventArgs args)
        {
            if (!pause && !crash)
            {
                frames++;
                if (j1.Mov) MovimentAnimacio(j1);
                else ResetAnimacio(j1);
                //if (j2.Mov) MovimentAnimacio(j2);
                //else ResetAnimacio(j2);
                if (frames == velAnimacio) frames = 0;
                Moviment();
                Gravetat();
                m_VideoSurface.Blit(m_BackgroundE, PosicioFonsE());
                m_VideoSurface.Blit(m_BackgroundD, PosicioFonsD());
                m_VideoSurface.Blit(ImatgeAnimacio(j1), PosicioJugador(j1), Animacio(j1));
                m_VideoSurface.Blit(myfont.Render(j1.PosX.ToString(new string('0', 3)) + ", " + j1.PosY.ToString(new string('0', 3)), Color.Red), new Point(400, 0));
                //if (j1.Terra) pause = true;
                //m_VideoSurface.Blit(ImatgeAnimacio(j2), PosicioJugador(j2), Animacio(j2));

                m_VideoSurface.Update();
            }
        }
        private void KeyboardDownEventHandler(object sender, KeyboardEventArgs args)
        {
            switch (args.Key)
            {
                // Jugador 1 al premer tecla.
                case Key.RightArrow:
                    //arrowRight = true;
                    j1.Direccio = 2;
                    break;
                case Key.LeftArrow:
                    //arrowLeft = true;
                    j1.Direccio = 0;
                    break;
                case Key.UpArrow:
                    arrowUp = true;
                    j1.Direccio = 1;
                    //if (j1.Doblesalt == 1)
                    {
                        j1.Terra = true;
                        j1.Salt = 0;
                        j1.VelCaiguda = 0;
                    }
                    break;
                case Key.DownArrow:
                    arrowDown = true;
                    break;
                // Jugador 2 al premer tecla.
                //case Key.D:
                //    //arrowD = true;
                //    j2.Direccio = 2;
                //    break;
                //case Key.A:
                //    //arrowA = true;
                //    j2.Direccio = 0;
                //    break;
                //case Key.W:
                //    arrowW = true;
                //    j2.Direccio = 1;
                //    //if (j2.Doblesalt == 1)
                //    {
                //        j2.Terra = true;
                //        j2.Salt = 0;
                //        j2.VelCaiguda = 0;
                //    }
                //    break;
                //case Key.S:
                //    arrowS = true;
                //    break;
                // Teclas generals al premer.
                case Key.Space:
                    if (!pause) { pause = true; }
                    else { pause = false; }
                    break;
                case Key.Escape:
                    Events.QuitApplication();
                    break;
            }
        }
        private void KeyboardUpEventHandler(object sender, KeyboardEventArgs args)
        {
            switch (args.Key)
            {
                // Jugador 1 al aixecar tecla.
                case Key.RightArrow:
                    arrowRight = false;
                    break;
                case Key.LeftArrow:
                    arrowLeft = false;
                    break;
                case Key.UpArrow:
                    j1.Doblesalt++;
                    break;
                case Key.DownArrow:
                    arrowDown = false;
                    break;
                // Jugador 2 al aixecar tecla.
                //case Key.D:
                //    arrowD = false;
                //    break;
                //case Key.A:
                //    arrowA = false;
                //    break;
                //case Key.W:
                //    j2.Doblesalt++;
                //    break;
                //case Key.S:
                //    arrowS = false;
                //    break;
                // Teclas generals al aixecar.
                case Key.Space:
                    break;
                case Key.Escape:
                    Events.QuitApplication();
                    break;
            }
        }
        private void Moviment()
        {
            j1.Mov = false;
            //j2.Mov = false;
            if (arrowRight) { j1.PosX += 1; j1.Mov = true; }
            if (arrowLeft) { j1.PosX -= 1; j1.Mov = true; }
            if (arrowUp)
            {
                if (j1.Salt != SALTMAX)
                {
                    j1.Mov = true;
                    j1.PosY -= 10;
                    j1.Salt += 1;
                    j1.Terra = false;
                }
            }
            if (j1.PosY + j1.S.DimImgY >= DIMY || j1.PosY <= 0) crash = true;
            //if (arrowD) { j2.PosX += 1; j2.Mov = true; }
            //if (arrowA) { j2.PosX -= 1; j2.Mov = true; }
            //if (arrowW)
            //{
            //    j2.Mov = true;
            //    if (j2.Salt != SALTMAX && j2.Doblesalt < 2)
            //    {
            //        j2.PosY -= 10;
            //        j2.Salt += 1;
            //        j2.Terra = false;
            //    }
            //    else if(j2.Salt != SALTMAX)
            //    {
            //        j2.PosY -= 10;
            //        j2.Salt += 1;
            //        j2.Terra = false;
            //    }
            //}
        }
        private void Gravetat()
        {
            if (!j1.Terra && j1.Salt == SALTMAX)
            {
                j1.VelCaiguda = j1.VelCaiguda + g * S;
                j1.PosY = j1.PosY + Convert.ToInt32(Math.Round(j1.VelCaiguda));
            }
            //if (!j2.Terra && j2.Salt == SALTMAX)
            //{
            //    j2.VelCaiguda = j2.VelCaiguda + g * S;
            //    j2.PosY = j2.PosY + Convert.ToInt32(Math.Round(j2.VelCaiguda));
            //    if (j2.PosY + j2.S.DimImgY >= DIMY)
            //    {
            //        j2.VelCaiguda = 0;
            //        j2.Terra = true;
            //        j2.Salt = 0;
            //        arrowW = false;
            //        j2.PosY = DIMY - j2.S.DimImgY;
            //        j2.Doblesalt = 0;
            //        j2.S.PosImgX = 0;
            //        j2.S.PosImgY = 0;
            //    }
            //}
        }
        private Rectangle Animacio(pj j)
        {
            Rectangle r;
            switch (j.Direccio)
            {
                case (int)Direccio.Left:
                    r = new Rectangle(new Point(j.E.PosImgX, j.E.PosImgY), new Size(j.E.DimImgX, j.E.DimImgY));
                    break;
                case (int)Direccio.Up:
                    r = new Rectangle(new Point(j.S.PosImgX, j.S.PosImgY), new Size(j.S.DimImgX, j.S.DimImgY));
                    break;
                default:
                    r = new Rectangle(new Point(j.D.PosImgX, j.D.PosImgY), new Size(j.D.DimImgX, j.D.DimImgY));
                    break;
            }
            return r;
        }
        private BaseSdlResource ImatgeAnimacio(pj j)
        {
            BaseSdlResource s;
            switch (j.Direccio)
            {
                case (int)Direccio.Left:
                    s = j.E.Img;
                    break;
                case (int)Direccio.Up:
                    s = j.S.Img;
                    break;
                default:
                    s = j.D.Img;
                    break;
            }
            return s;
        }
        private Point PosicioFonsE()
        {
            if (posFonsE == -800) posFonsE = 800;
            else posFonsE-= 1;
            return new Point(posFonsE, 0);
        }
        private Point PosicioFonsD()
        {
            if (posFonsD == -800) posFonsD = 800;
            else posFonsD -= 1;
            return new Point(posFonsD, 0);
        }
        private void MovimentAnimacio(pj j)
        {
            if(frames == velAnimacio)
            {
                switch (j.Direccio)
                {
                    case (int)Direccio.Left:
                        if (j.E.PosImgX != j.E.DimObjX - j.E.DimImgX && j.E.DimImgX != j.E.DimObjX) j.E.PosImgX += j.E.DimImgX;
                        else j.E.PosImgX = 0;
                        break;
                    case (int)Direccio.Up:
                        if (j.S.PosImgX != j.S.DimObjX - j.S.DimImgX && j.S.DimImgX != j.S.DimObjX) j.S.PosImgX += j.S.DimImgX;
                        else
                        {
                            j.S.PosImgX = 0;
                            if (j.S.PosImgY != j.S.DimObjY - j.S.DimImgY && j.S.DimImgY != j.S.DimObjY) j.S.PosImgY += j.S.DimImgY;
                            else j.S.PosImgY = 0;
                        }
                        break;
                    default:
                        if (j.D.PosImgX != j.D.DimObjX - j.D.DimImgX && j.D.DimImgX != j.D.DimObjX) j.D.PosImgX += j.D.DimImgX;
                        else j.D.PosImgX = 0;
                        break;
                }
            }
        }
        private void ResetAnimacio(pj j)
        {
            switch (j.Direccio)
            {
                case (int)Direccio.Left:
                    j.E.PosImgX = 0;
                    j.E.PosImgY = 0;
                    break;
                case (int)Direccio.Right:
                    j.D.PosImgX = 0;
                    j.D.PosImgY = 0;
                    break;
            }
        }
        private Point PosicioJugador(pj j)
        {
            return new Point(j.PosX, j.PosY);
        }
    }
}
