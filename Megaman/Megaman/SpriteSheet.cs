﻿using System;
using System.Drawing;
using System.Runtime.InteropServices;
using SdlDotNet.Graphics;
using SdlDotNet.Core;
using SdlDotNet.Input;


namespace Megaman
{
    class SpriteSheet
    {
        private Surface img;

        private int posImgX;
        private int posImgY;
        private int dimObjX;
        private int dimObjY;
        private int dimImgX;
        private int dimImgY;
        private int posX;
        private int posY;

        public SpriteSheet(Surface img, int dimObjX, int dimObjY, int dimImgX, int dimImgY)
        {
            this.img = img;
            this.dimObjX = dimObjX;
            this.dimObjY = dimObjY;
            this.dimImgX = dimImgX;
            this.dimImgY = dimImgY;
            posImgX = 0;
            posImgY = 0;
        }
        #region Propietats
        public Surface Img
        {
            get { return img;  }
            set { img = value; }
        }
        public int PosImgX
        {
            get { return posImgX; }
            set { posImgX = value; }
        }
        public int PosImgY
        {
            get { return posImgY; }
            set { posImgY = value; }
        }
        public int DimObjX
        {
            get { return dimObjX; }
            set { dimObjX = value; }
        }
        public int DimObjY
        {
            get { return dimObjY; }
            set { dimObjY = value; }
        }
        public int DimImgX
        {
            get { return dimImgX; }
            set { dimImgX = value; }
        }
        public int DimImgY
        {
            get { return dimImgY; }
            set { dimImgY = value; }
        }
        #endregion
    }
}
