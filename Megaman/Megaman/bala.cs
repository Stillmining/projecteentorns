﻿using System;
using System.Drawing;
using System.Runtime.InteropServices;
using SdlDotNet.Graphics;
using SdlDotNet.Core;
using SdlDotNet.Input;

namespace Megaman
{
    class bala
    {
        private SpriteSheet img;
        private double velocitat;
        private int direccio;

        public bala(SpriteSheet img, int direccio)
        {
            this.img = img;
            velocitat = 0;
            this.direccio = direccio;
        }

        #region Propietats
        public SpriteSheet Img
        {
            get { return img; }
            set { img = value; }
        }
        public double Velocitat
        {
            get { return velocitat; }
            set { velocitat = value; }
        }
        public int Direccio
        {
            get { return direccio; }
            set { velocitat = value; }
        }
        #endregion

    }
}
