﻿using System;
using System.Drawing;
using System.Runtime.InteropServices;
using SdlDotNet.Graphics;
using SdlDotNet.Core;
using SdlDotNet.Input;

namespace Megaman
{
    class pj
    {
        private SpriteSheet d;
        private SpriteSheet e;
        private SpriteSheet s;
        private int balas;
        private int posX;
        private int posY;
        private int salt;
        private double velCaiguda;
        private bool terra;
        private bool baixa;
        private bool puja;
        private bool mov;
        private int doblesalt;
        private int posDef;
        private int direccio;
        



        public pj(SpriteSheet d, SpriteSheet e, SpriteSheet s, int posDef)
        {
            this.d = d;
            this.e = e;
            this.s = s;
            this.posDef = posDef;
            baixa = false;
            puja = false;
            terra = false;
            salt = 0;
            velCaiguda = 0;
            doblesalt = 0;
            balas = 100;
            posX = 0;
            posY = 0;
            mov = false;
        }
        public pj(SpriteSheet d, SpriteSheet e, SpriteSheet s, int posDef, int posX, int posY)
        {
            this.d = d;
            this.e = e;
            this.s = s;
            this.posX = posX;
            this.posY = posY;
            this.posDef = posDef;
            baixa = false;
            puja = false;
            terra = true;
            salt = 0;
            velCaiguda = 0;
            doblesalt = 0;
            balas = 100;
            mov = false;
        }
        #region Propietats
        public SpriteSheet D
        {
            get { return d; }
            set { d = value; }
        }
        public SpriteSheet E
        {
            get { return e; }
            set { e = value; }
        }
        public SpriteSheet S
        {
            get { return s; }
            set { s = value; }
        }
        public bool Baixa
        {
            get { return baixa; }
            set { baixa = value; }
        }
        public bool Puja
        {
            get { return puja; }
            set { puja = value; }
        }
        public bool Terra
        {
            get { return terra; }
            set { terra = value; }
        }
        public int Doblesalt
        {
            get { return doblesalt; }
            set { doblesalt = value; }
        }
        public int Balas
        {
            get { return balas; }
            set { balas = value; }
        }
        public int PosX
        {
            get { return posX; }
            set { posX = value; }
        }
        public int PosY
        {
            get { return posY; }
            set { posY = value; }
        }
        public double VelCaiguda
        {
            get { return velCaiguda; }
            set { velCaiguda = value; }
        }
        public int Salt
        {
            get { return salt; }
            set { salt = value; }
        }
        public int PosDef
        {
            get { return posDef; }
            set { posDef = value; }
        }
        public bool Mov
        {
            get { return mov; }
            set { mov = value; }
        }
        public int Direccio
        {
            get { return direccio; }
            set { direccio = value; }
        }
        #endregion
    }
}
