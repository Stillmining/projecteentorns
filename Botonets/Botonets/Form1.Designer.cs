﻿namespace Botonets
{
    partial class frmBotons
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnPrimer = new System.Windows.Forms.Button();
            this.btnSegon = new System.Windows.Forms.Button();
            this.btnTercer = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnPrimer
            // 
            this.btnPrimer.Location = new System.Drawing.Point(66, 66);
            this.btnPrimer.Name = "btnPrimer";
            this.btnPrimer.Size = new System.Drawing.Size(75, 23);
            this.btnPrimer.TabIndex = 0;
            this.btnPrimer.Text = "Primer Boto";
            this.btnPrimer.UseVisualStyleBackColor = true;
            // 
            // btnSegon
            // 
            this.btnSegon.Location = new System.Drawing.Point(66, 96);
            this.btnSegon.Name = "btnSegon";
            this.btnSegon.Size = new System.Drawing.Size(75, 23);
            this.btnSegon.TabIndex = 1;
            this.btnSegon.Text = "Segon Boto";
            this.btnSegon.UseVisualStyleBackColor = true;
            // 
            // btnTercer
            // 
            this.btnTercer.Location = new System.Drawing.Point(66, 126);
            this.btnTercer.Name = "btnTercer";
            this.btnTercer.Size = new System.Drawing.Size(75, 23);
            this.btnTercer.TabIndex = 2;
            this.btnTercer.Text = "Tercer Boto";
            this.btnTercer.UseVisualStyleBackColor = true;
            // 
            // frmBotons
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(491, 371);
            this.Controls.Add(this.btnTercer);
            this.Controls.Add(this.btnSegon);
            this.Controls.Add(this.btnPrimer);
            this.Name = "frmBotons";
            this.Text = "Botons Nets";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnPrimer;
        private System.Windows.Forms.Button btnSegon;
        private System.Windows.Forms.Button btnTercer;
    }
}

